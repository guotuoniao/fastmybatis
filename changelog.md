# 更新日志

## 1.0.15

- 代码优化性能优化（阿里代码规范+sonar）

## 1.0.14

- 代码优化（sonar）

## 1.0.13

- 合并PR2，https://gitee.com/durcframework/fastmybatis/pulls/2

## 1.0.12

- 修复模板问题


## 1.0.11

- 优化属性拷贝

## 1.0.10

- 增强Mapper.xml，不同Mapper文件可指定同一个namespace，最终会合并

## 1.0.9

- 对象转换方法优化

## 1.0.8

- pageSize传0时报错bug

## 1.0.7

- 参数类构建条件可以获取父类属性

## 1.0.6

- EasyuiDtagridParam增加条件忽略

## 1.0.5

- easyui表格参数支持

## 1.0.4

- Mapper对应无实体类BUG

## 1.0.3

- 添加强制查询功能，见TUserMapperTest.testForceQuery()

## 1.0.2

- 完善注释
- 代码优化

## 1.0.1 初版